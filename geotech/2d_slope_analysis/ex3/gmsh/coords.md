Three nodes to monitor displacement:
starting node: 1; 1, 3, 5, .... etc line 15
ending node: 3967; line 1039
number of nodes: 1039 - 15 + 1 = 1025
Toe of slope: 1009 (ndf=3) line 278, 264
Top of slope: 1755 line 463, 449
Middle of the slope:
Far from the slope: 3511 line 923, 909

Left boundary:
Top nodes: 3967, 3910, 3853
Bottom nodes: 3911, 3854, 3797

Slope begins:
left top: 2713
right top: 1755

right slope: 1755, 1723, 1680, 1634, 1597,

top of the slope: 1009

Coordinates from LibreCad
top left: (34.6785, 246.9706) Periodic bound start: (131.4410, 246.9706)
Start of slope: (556.4691, 246.9706), (582.6471, 238.7432), (633.2157, 229.4408), (699.8674, 217.9898)
Right top of layer: Periodic boundary start: (895.1197, 217.9898), (992.2500, 217.9898)
Right bottom of layer: (992.2500, 129.7641), Periodic boundary: (895.1197, 129.7641), (131.4410, 129.7641), Left edge: (34.6785, 129.7641)

right boundary
node        1     544.068    -68.580
node       78     544.068    -12.802
node      210     483.108    -12.802
node       84     483.108    -68.580

Slope
node     1755     266.700      4.877
node     1680     275.822      2.357
node     1597     284.988      0.000
node     1449     303.276     -2.896
node     1301     321.564     -5.791
node     1201     334.772     -8.128
node     1105     347.980    -10.465
node     1009     361.188    -12.802

node     2713     158.490      4.877
node     2699     158.490    -12.802
node     2650     158.490    -68.580


Left boundary
node     3911     -60.960    -68.580
node     3797       0.000    -68.580
node     3853       0.000      4.877
node     3967     -60.960      4.877
