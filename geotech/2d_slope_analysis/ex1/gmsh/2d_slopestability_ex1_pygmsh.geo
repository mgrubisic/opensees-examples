// This code was created by pygmsh vunknown.
p0 = newp;
Point(p0) = {0, 0, 0, 1.0};
p1 = newp;
Point(p1) = {15.0, 0, 0, 1.0};
p2 = newp;
Point(p2) = {15.0, 10.0, 0, 1.0};
p3 = newp;
Point(p3) = {0, 10.0, 0, 1.0};
p4 = newp;
Point(p4) = {0, -20.0, 0, 1.0};
p5 = newp;
Point(p5) = {15.0, -20.0, 0, 1.0};
p6 = newp;
Point(p6) = {67.0, 0, 0, 1.0};
p7 = newp;
Point(p7) = {37.0, 10.0, 0, 1.0};
p8 = newp;
Point(p8) = {67.0, -20.0, 0, 1.0};
p9 = newp;
Point(p9) = {97.0, 0, 0, 1.0};
p10 = newp;
Point(p10) = {97.0, -20.0, 0, 1.0};
p11 = newp;
Point(p11) = {112.0, 0, 0, 1.0};
p12 = newp;
Point(p12) = {112.0, -20.0, 0, 1.0};
l0 = newl;
Line(l0) = {p0, p1};
l1 = newl;
Line(l1) = {p1, p2};
l2 = newl;
Line(l2) = {p2, p3};
l3 = newl;
Line(l3) = {p3, p0};
l4 = newl;
Line(l4) = {p0, p4};
l5 = newl;
Line(l5) = {p4, p5};
l6 = newl;
Line(l6) = {p5, p1};
l7 = newl;
Line(l7) = {p1, p6};
l8 = newl;
Line(l8) = {p6, p7};
l9 = newl;
Line(l9) = {p7, p2};
l10 = newl;
Line(l10) = {p5, p8};
l11 = newl;
Line(l11) = {p8, p6};
l12 = newl;
Line(l12) = {p8, p10};
l13 = newl;
Line(l13) = {p10, p9};
l14 = newl;
Line(l14) = {p9, p6};
l15 = newl;
Line(l15) = {p10, p12};
l16 = newl;
Line(l16) = {p12, p11};
l17 = newl;
Line(l17) = {p11, p9};
ll0 = newll;
Line Loop(ll0) = {l0, l1, l2, l3};
ll1 = newll;
Line Loop(ll1) = {l5, l6, -l0, l4};
ll2 = newll;
Line Loop(ll2) = {l7, l8, l9, -l1};
ll3 = newll;
Line Loop(ll3) = {l10, l11, -l7, -l6};
ll4 = newll;
Line Loop(ll4) = {l12, l13, l14, -l11};
ll5 = newll;
Line Loop(ll5) = {l15, l16, l17, -l13};
s0 = news;
Plane Surface(s0) = {ll0};
s1 = news;
Plane Surface(s1) = {ll1};
s2 = news;
Plane Surface(s2) = {ll2};
s3 = news;
Plane Surface(s3) = {ll3};
s4 = news;
Plane Surface(s4) = {ll4};
s5 = news;
Plane Surface(s5) = {ll5};
Transfinite Line {l0, l2} = 11;
Transfinite Line {l1, l3} = 11;
Transfinite Surface {s0};
Transfinite Line {l5, -l0} = 11;
Transfinite Line {l6, l4} = 11;
Transfinite Surface {s1};
Transfinite Line {l7, l9} = 21;
Transfinite Line {l8, -l1} = 11;
Transfinite Surface {s2};
Transfinite Line {l10, -l7} = 21;
Transfinite Line {l11, -l6} = 11;
Transfinite Surface {s3};
Transfinite Line {l12, l14} = 11;
Transfinite Line {l13, -l11} = 11;
Transfinite Surface {s4};
Transfinite Line {l15, l17} = 11;
Transfinite Line {l16, -l13} = 11;
Transfinite Surface {s5};
Recombine Surface {s0};
Recombine Surface {s1};
Recombine Surface {s2};
Recombine Surface {s3};
Recombine Surface {s4};
Recombine Surface {s5};
