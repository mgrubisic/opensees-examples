# OpenSees Example
These examples are my effort to learn OpenSees. I have either copied verbatim from the OpenSees tutorials or modified a little using Python modules. These examples use the excellent [OpenSeesPy Library](https://github.com/zhuminjie) by Minjie Zhu. Anyone looking to learn OpenSees and using Python should check this library. I will upload files as and when I complete a particular example.

# Requirements
To use OpenSeesPy only Python 3 and OpenseesPy are necessary. I installed additional packages for ease of data manipulation and plotting.
1. Python 3
2. OpenSeesPy
3. Numpy
4. Matplotlib
5. Pandas
6. svgwrite - this is not absolutely essential. I used this to draw the soil profile in 1D consolidation. I may use this to draw frame structures in future.


# Resources:
1. [OpenSees Website](https://opensees.berkeley.edu/)
2. [OpenSees Examples](https://opensees.berkeley.edu/wiki/index.php/Examples)
3. [OpenSeesPy](https://github.com/zhuminjie)
4. [OpenSeesPy Examples](https://openseespydoc.readthedocs.io/en/latest/src/examples.html)